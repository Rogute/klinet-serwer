import socket

SERVER = socket.gethostbyname(socket.gethostname())
PORT = 8081

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((SERVER, PORT))

    while True:
        command = input('Type what you want: ').encode('utf-8')
        s.send(command)
        data = s.recv(1024).decode('utf-8')
        print(data)

        if str(command) == "stop" or not data:
            print("BYE BYE")
            break
