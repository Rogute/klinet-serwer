from datetime import datetime
import json
import socket
import time


SERVER = socket.gethostbyname(socket.gethostname())
PORT = 8081
FORMAT = "utf-8"
START = datetime.now()


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((SERVER, PORT))
    s.listen()
    print("[STARTING] ...")
    print(f"[LISTENING] Server is listening on {SERVER}")
    conn, addr = s.accept()
    with conn:
        print(f"[NEW CONNECTION] {addr} connected.")
        while True:
            command = conn.recv(1024).decode(FORMAT)
            if command == "info":
                conn.send(json.dumps({"server_version": "1.1",
                                      "server_started": START.strftime("%d-%m-%Y %H:%M:%S")
                                      }, indent=2).encode(FORMAT))
            elif command == "help":
                conn.send(json.dumps({"uptime": "Returns server life time",
                                      "info": "Returns server version and creat date",
                                      "help": "Returns list of available command",
                                      "stop": "Stops both client and server"
                                      }, indent=4).encode(FORMAT))
            elif command == "uptime":
                date_now = datetime.now()
                uptime = (date_now - START).total_seconds()
                conn.send(json.dumps({"server life time": uptime}).encode(FORMAT))
            elif command == "stop":
                break
            else:
                conn.send(json.dumps({"ERROR": "type 'help'"}).encode(FORMAT))
    s.close()
